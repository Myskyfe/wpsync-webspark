<?php

/**
 * Handles the processes
 */
class Handler extends Api
{
    /**
     * @return void
     */
    public static function initialize()
    {
        self::sync($ek);
    }

    /**
     * @return void
     */
    public function sync()
    {
        $response = self::getData();

        if ($response != null) {
            global $wpdb;
            $data = $response->data;
            $count_data = count($data);

            if($count_data > 2000)
                $data = array_slice($data, 0, 2000);

            $products = $wpdb->get_results("select post_id, meta_key, meta_value from $wpdb->postmeta 
                                                   inner join $wpdb->posts
                                                   on $wpdb->postmeta.post_id = $wpdb->posts.ID
                                                   where post_type = 'product' and
                                                   $wpdb->posts.post_status = 'publish' and 
                                                   $wpdb->postmeta.meta_key = '_sku'");

            foreach ($products as $product){
                if(!self::findObjectBySku($product->meta_value, $data)){
                    wp_delete_post($product->post_id);
                }
                sleep(1);
            }

            foreach ($data as $key => $elem) {
                $result = $wpdb->get_results( "select * from $wpdb->posts
                                        inner join $wpdb->postmeta
                                        on $wpdb->posts.ID = $wpdb->postmeta.post_id
                                        where $wpdb->postmeta.meta_key = '_sku' and
                                              $wpdb->postmeta.meta_value = '{$elem->sku}' and
                                              $wpdb->posts.post_status = 'publish'" );
                if(!$result){
                    self::createProduct($elem);
                } else {
                    self::updateProduct($result->ID, $elem);
                }

                sleep(1);
            }

        }
    }

    /**
     * @param $data
     * @return void
     */
    private function createProduct($data)
    {
        $post_id = wp_insert_post([
            'post_title' => $data->name,
            'post_excerpt' => $data->description,
            'post_type' => 'product',
            'post_status' => 'publish'
        ]);

        self::postMetaUpdate($post_id, $data);
        self::setThumbnail($post_id, $data->picture);
    }

    /**
     * @param $post_id
     * @param $data
     * @return void
     */
    private function updateProduct($post_id, $data)
    {
        $the_post = array(
            'ID' => $post_id,
            'post_title' => $data->name,
            'post_excerpt' => $data->description,
        );

        wp_update_post($the_post);

        self::postMetaUpdate($post_id, $data);
    }

    /**
     * @param $post_id
     * @param $image_url
     * @return void
     */
    private function setThumbnail($post_id, $image_url)
    {
        $image_name = 'product-' . rand(0, 1000) * 10 . '.jpg';
        $upload_dir = wp_upload_dir(); // Set upload folder
        $image_data = file_get_contents($image_url); // Get image data
        $unique_file_name = wp_unique_filename($upload_dir['path'], $image_name); // Generate unique name
        $filename = basename($unique_file_name); // Create image file name

        if (wp_mkdir_p($upload_dir['path'])) {
            $file = $upload_dir['path'] . '/' . $filename;
        } else {
            $file = $upload_dir['basedir'] . '/' . $filename;
        }

        file_put_contents($file, $image_data);

        $wp_filetype = wp_check_filetype($filename, null);
        $attachment = array(
            'post_mime_type' => $wp_filetype['type'],
            'post_title' => sanitize_file_name($filename),
            'post_content' => '',
            'post_status' => 'inherit'
        );
        $attach_id = wp_insert_attachment($attachment, $file, $post_id);

        require_once(ABSPATH . 'wp-admin/includes/image.php');

        $attach_data = wp_generate_attachment_metadata($attach_id, $file);

        wp_update_attachment_metadata($attach_id, $attach_data);
        set_post_thumbnail($post_id, $attach_id);
    }

    /**
     * @param $post_id
     * @param $data
     * @return void
     */
    private function postMetaUpdate($post_id, $data)
    {
        $price = explode('$', $data->price)[1];
        $update_data = [
            '_visibility' => 'visible',
            '_stock_status' => $data->in_stock == 0 ? 'outofstock' : 'instock',
            'total_sales' => '0',
            '_downloadable' => 'no',
            '_virtual' => 'yes',
            '_regular_price' => $price,
            '_price' => $price,
            '_sale_price' => '',
            '_purchase_note' => '',
            '_featured' => 'no',
            '_sku' => $data->sku,
            '_product_attributes' => array(),
            '_sale_price_dates_from' => '',
            '_sale_price_dates_to' => '',
            '_sold_individually' => '',
            '_manage_stock' => 'yes',
            '_backorders' => 'no',
        ];

        foreach ($update_data as $key => $update_elem) {
            update_post_meta($post_id, $key, $update_elem);
        }

        wc_update_product_stock($post_id, $data->in_stock, 'set'); // set 1000 in stock

    }

    /**
     * @param $sku
     * @param $array
     * @return object|false
     */
    private function findObjectBySku($sku, $array)
    {
        foreach ($array as $element) {
            if ($sku == $element->sku) {
                return $element;
            }
        }

        return false;
    }
}
