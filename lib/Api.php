<?php

/**
 *  Implements an API interface
 */
class Api
{
    /**
     * Api handling url
     */
    const API_URL = 'https://wp.webspark.dev/wp-api/products';

    /**
     * @return mixed|null
     */
    public function getData(){
        $handle = curl_init(self::API_URL);

        curl_setopt($handle,  CURLOPT_RETURNTRANSFER, TRUE);

        $response = curl_exec($handle);
        $httpCode = curl_getinfo($handle, CURLINFO_HTTP_CODE);

        //server response checking
        if($httpCode == 200) {
            return json_decode($response);
        }else{
            return null;
        }
    }
}