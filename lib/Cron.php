<?php

/**
 * Register cron events
 */
class Cron
{
    /**
     * @return void
     */
    public static function regEvent(){

        add_action( 'admin_head', 'scheduled_event' );
        function scheduled_event() {
            if( ! wp_next_scheduled( 'hourly_event' ) ) {
                wp_schedule_event( time(), 'hourly', 'hourly_event');
            }
        }

        add_action( 'hourly_event', 'do_this_hourly' );
        function do_this_hourly(){
            Handler::initialize();
        }
    }
}