<?php
/*
Plugin Name: WPSYNC Webspark
Plugin URI: https://modeeffect.com/
Description: Synchronization of the database of products with stats
Version: 0.1
Author: Matvii Yermakov
Text Domain: wpsync-webspark
*/

const PLUGIN_DIR = __DIR__;

include_once('includes/Kernel.php');
Kernel::initialize();
