<?php

/**
 * Kernel initializing class
 */
class Kernel
{
    /**
     * @return void
     */
    public static function initialize(){
        (new Kernel)->libs();
        (new Kernel)->actions();
    }

    /**
     * @return void
     */
    private function libs(){
        include_once(PLUGIN_DIR."/lib/Api.php");
        include_once(PLUGIN_DIR."/lib/Handler.php");
        include_once(PLUGIN_DIR."/lib/Cron.php");
    }

    /**
     * @return void
     */
    private function actions(){
        Cron::regEvent();
    }
}